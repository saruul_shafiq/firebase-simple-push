# Firebase Simple Push #

Ever needed to create values with auto generated keys in your Firebase real time database manually ? Here is the solution you have to set up once for every Firebase project which enables you to push values manually to your [Firebase Realtime Database](https://firebase.google.com/docs/database/).

### Prerequisites ###

Before you start be sure to satisfy that you ...

* have a Firebase project,
* have the [Firebase CLI](https://firebase.google.com/docs/cli/) installed and
* are not using [Firebase Hosting](https://firebase.google.com/docs/hosting/) already as we will use it for our push Code.

### Installing ###

## Get the code ##

Clone the repository in the directory of your choice.
```
#!git

git clone https://saruul_shafiq@bitbucket.org/saruul_shafiq/firebase-simple-push.git
```

## Create a user ##

To avoid that anybody is pushing to your database we will create a user manually which will act as kind of an admin. If you already have a user registered you want to use (like your own account) you can skip this step. 
Go to the [authentication menu of your project](https://console.firebase.google.com/project/trainplan-4105b/authentication/users) and add a new user. Use any email address and password you want. We will use the User UID to just allow one user to push to your database. Of course you can have your own [Firebase database rules](https://firebase.google.com/docs/database/security/) but be sure that this user is allowed to write to the database path you like to push data to.

## Get Firebase credentials for your web app ##